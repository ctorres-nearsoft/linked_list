"""Testing module."""

import unittest

from DoubleLinkedList import DoubleLinkedList as DLL


class TestDLL(unittest.TestCase):
    """Test case for the DoubleLinkedList."""

    def test_append(self):
        """Test case for the append."""

        d = DLL()

        #When list is empty
        d.append(1)
        self.assertEqual(d.length, 1)
        self.assertEqual(d[0], 1)

        #When list has 1 element
        d.append(2)
        self.assertEqual(d.length, 2)
        self.assertEqual(d[1], 2)

        #When list has 2 elements
        d.append(3)
        self.assertEqual(d.length, 3)
        self.assertEqual(d[2], 3)

    def test_remove(self):
        """Test case for the remove."""

        d = DLL()

        d.append(1)
        d.append(2)
        d.append(3)
        d.append(4)
        d.append(5)

        #From start
        d.remove(0)
        self.assertEqual(d.length, 4)
        self.assertEqual(d[0], 2)

        #From end
        d.remove(3)
        self.assertEqual(d.length, 3)
        self.assertEqual(d[2], 4)

        #From middle
        d.remove(1)
        self.assertEqual(d.length, 2)
        self.assertEqual(d[1], 4)

        #When there's only one element
        d.remove(0)
        d.remove(0)
        self.assertEqual(d.length, 0)

    def test_insert(self):
        """Test case for the insert."""

        d = DLL()

        #Empty
        d.insert(0, 1)
        self.assertEqual(d.length, 1)
        self.assertEqual(d[0], 1)

        #Start
        d.insert(0, 0)
        self.assertEqual(d.length, 2)
        self.assertEqual(d[0], 0)

        #End
        d.insert(2, 2)
        self.assertEqual(d.length, 3)
        self.assertEqual(d[2], 2)

        #Reverse
        d.insert(-1, 3)
        self.assertEqual(d.length, 4)
        self.assertEqual(d[-1], 3)

        #Out of bounds
        self.assertIsNone(d.insert(10, 10))
        self.assertIsNone(d.insert(-10, 10))

    def test_get_node(self):
        """Test case for the get_node."""

        d = DLL()

        #Empty list
        self.assertIsNone(d.get_node(0))

        d.append(1)

        #In of bounds
        self.assertEqual(d.get_node(-1).content, 1)
        self.assertEqual(d.get_node(0).content, 1)

        #Out of bounds
        self.assertIsNone(d.get_node(-2))
        self.assertIsNone(d.get_node(1))

    def test_index_of(self):
        """Test case for the index_of."""

        d = DLL()

        #Empty
        self.assertIsNone(d.index_of(1))

        d.append(1)

        #Non-existent
        self.assertIsNone(d.index_of(2))

        #Existent
        self.assertEqual(d.index_of(1), 0)

        d.append(1)

        #Duplicate
        self.assertEqual(d.index_of(1), 0)

    def test_is_empty(self):
        """Test case for the is_empty."""

        d = DLL()

        #Empty first time
        self.assertTrue(d.is_empty())

        d.append(1)

        #Not empty
        self.assertFalse(d.is_empty())

        d.remove(0)

        #Empty after remove
        self.assertTrue(d.is_empty())

    def test_len(self):
        """Test case for the len magic method."""

        d = DLL()

        #len = 0
        self.assertEqual(len(d), 0)

        d.append(1)

        #len = 1
        self.assertEqual(len(d), 1)

        d.append(2)

        #len = 2
        self.assertEqual(len(d), 2)

        d.remove(0)

        #len = 1 after remove
        self.assertEqual(len(d), 1)

if __name__ == '__main__':

    unittest.main()
