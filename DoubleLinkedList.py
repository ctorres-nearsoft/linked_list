"""DoubleLinkedList module.

This is the documentation for the DoubleLinkedList.
"""


class DoubleLinkedList:
    """The main class for the DoubleLinkedList."""

    class Node:
        """This is an auxiliary class."""

        def __init__(self, content=None, _next=None, previous=None):

            self.content = content
            self._next = _next
            self.previous = previous

    def __init__(self):

        self.head = None
        self.tail = None
        self.size = 0
        self.index = 0

    def append(self, content):
        """Appends an item at the end of the list.

        >>> linked_list.append(5)
        >>> print(linked_list)
        ... [5]
        """

        if self.head is None:

            self.head = DoubleLinkedList.Node(content)
            self.tail = self.head

        else:

            self.tail._next = DoubleLinkedList.Node(content, None, self.tail)
            self.tail = self.tail._next

        self.size += 1

    def insert(self, index, content):
        """Inserts an item in whichever index is given."""

        if index > self.size or index < -self.size:

            return None

        if index < 0:

            index = self.size + index + 1

        if index == self.size or self.head is None:

            self.append(content)

        elif index == 0:

            self.head.previous = DoubleLinkedList.Node(content, self.head)
            self.head = self.head.previous

            self.size += 1

        else:

            current = self.get_node(index)

            before = current.previous

            before._next = DoubleLinkedList.Node(content, before, current)
            current.previous = before._next

            self.size += 1

    def get_node(self, index):
        """Gets the item at the given index."""

        if index >= self.size or index < -self.size:

            return None

        if index < 0:

            index = self.size + index

        current = None

        if index < self.size // 2:

            current = self.head

            for i in range(index):

                current = current._next

        else:

            current = self.tail

            for i in range(self.size - 1 - index):

                current = current.previous

        return current

    def index_of(self, content):
        """Gets the item with the given content.

        .. note::
            In case there is a repeated item, it will return the first one from
            left to right.
        """

        for index, node in enumerate(self):

            if node == content:

                return index

        return None

    def remove(self, index):
        """Removes the item at the given index."""

        current = self.get_node(index)

        if current is None:

            return None

        if current is self.head and current is self.tail:

            self.head = self.tail = None

        elif current is self.head:

            after = self.head._next
            after.previous = None

            self.head = after

        elif current is self.tail:

            before = self.tail.previous
            before._next = None

            self.tail = before

        else:

            before = current.previous
            after = current._next

            before._next = after
            after.previous = before

        self.size -= 1

    def is_empty(self):
        """Checks if the DoubleLinkedList is empty."""

        return self.head is None

    @property
    def length(self):
        """This property is related to the size instance variable."""

        return self.size

    def __len__(self):

        return self.size

    def __getitem__(self, key):

        if key >= self.size or key < -self.size:

            raise IndexError

        return self.get_node(key).content

    def __iter__(self):

        current = self.head

        while current is not None:

            content = current.content
            current = current._next

            yield content

    def __str__(self):

        _list = []

        for i in range(self.size):

            _list.append(self.get_node(i).content)

        return str(_list)
